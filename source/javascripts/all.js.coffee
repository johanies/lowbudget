//= require "jquery"
//= require "d3"
//= require "topojson"
//= require "planetary.js/dist/planetaryjs"

      
$ ->
  $(window).on 'scroll', ->
    if $(window).scrollTop() >= $('#logo').height()
      $('#nav').addClass 'fixed'
    else
      $('#nav').removeClass 'fixed'
      
  
  
  $('[data-page-links] a').on 'click', (e) ->
    e.preventDefault()
    hash = $(this).attr('href')
    target = $(hash)
    
    console.log target.offset().top
    
    $('body, html').animate
      scrollTop: target.offset().top
    , 250, ->
      document.location.hash = hash